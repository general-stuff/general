/*!
 * \file cbuffer.h 
 * \brief general purpose circular buffer
 *
 * \author Droessler, Tobias
 * \date 30.04.2013
 ************************************************/ 

#include <stdint.h>


 /* == DEFINEs == */

#define BUFFER_SIZE 1024 				//  2^n (8, 16, 32, 64 ...)
#define BUFFER_MASK (BUFFER_SIZE-1) 	//	 

#define SUCCESS 	(0)
#define FAIL		((uint8_t) -1 )

/* == PROTOTYPEs == */


uint8_t buffIn(uint8_t byte); 
uint8_t buffOut(uint8_t *pByte);
uint16_t buffGetLevel(void);