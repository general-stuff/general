 

#include <stdint.h>
#include <stdio.h>


void gen_poly(void);
void generate_gf(void);
void encode_rs(void);
void decode_rs(void);
static uint8_t setNextNibble(uint8_t v);
static uint8_t getNextNibble(void);
void initSetNibble(uint8_t *pStart, uint8_t hiLoState) ;
void initGetNibble(uint8_t *pStart, uint8_t hiLoState) ;
uint32_t calc_encodedLengthShortcutedRS(uint32_t decodedLen);
// internal global variables //

static uint8_t internalGet_hiLoState=0; 
static uint8_t *internalGet_pData = NULL;
static uint8_t internalSet_hiLoState=0; 
static uint8_t *internalSet_pData = NULL;
static uint16_t lenInAbs=0,lenOutAbs=0;
static uint16_t lenInAvail=0;
static uint16_t lenOutAvail=0;
static uint16_t lenInPend=0;
//static uint16_t lenOutPend=0;

static int decodingErrStat_syn_error = 0;
static int decodingErrStat_err = 0;

#define START_WITH_MS_NIBBLE /// Start nibble split with the sost significant nibble
#include <math.h>

#define mm  4           /* RS code over GF(2**4) - change to suit */
#define nn  15          /* nn=2**mm -1   length of codeword */
#define tt  3           /* number of errors that can be corrected */
#define kk  9           /* kk = nn-2*tt  */

int pp [mm+1] = { 1, 1, 0, 0, 1} ; /* specify irreducible polynomial coeffts */
int alpha_to [nn+1], index_of [nn+1], gg [nn-kk+1] ;
int recd [nn], data [kk], bb [nn-kk] ;


// blockwise en/decosing to reduce delay //

void rs_initDecoding(uint8_t *pInStart, uint8_t lenIn, uint8_t *pOutStart)
{
  initGetNibble(pInStart, 0);  // set internal vars for nibble get method
  initSetNibble(pOutStart, 0);
}

void rs_initEncoding(uint8_t *pInStart, uint8_t lenIn, uint8_t *pOutStart)
{
  initGetNibble(pInStart, 0);  // set internal vars for nibble get method
  initSetNibble(pOutStart, 0);
  lenInAbs = lenIn*2;
  lenOutAbs = calc_encodedLengthShortcutedRS(lenIn)*2;
  lenInAvail = lenOutAvail= 0;
  
  uint8_t *pC = pOutStart;
  //clear buffer pOutStart
  for (uint8_t i=0; i< lenOutAbs/2; i++)
    *pC++ = 0;
}

/**
 *  \brief splits data into nibbles and call the encoder for one block
 *  \note  a call rs_initEncoding must init. this method.
 *  \sa rs_initEncoding
 *  \param pointer to start of input uint8_t data array
 *  \param pointer to output buffer 
 *  \return number of bytes to transmit from ouput puffer
 ********/ 
uint8_t dataInAndEncode(uint8_t len)
{
  uint32_t k;
  uint8_t retval=0;
  
  lenInAvail += len*2;
 // printf(":");
  if (lenInAvail > lenInAbs)
  {
    lenInPend += (lenInAvail - lenInAbs); 
  }
  else 
  {  
    lenInPend += len*2;
  }
  
  if (lenInAvail > lenInAbs)
    lenInAvail = lenInAbs;

  while((lenInPend >= kk) || (lenInAvail >= lenInAbs))
  {
    //printf("\n");
    for (k=0; k< kk; k++)
    {
      if (lenInPend)
      {
        data[k] = getNextNibble();
        lenInPend--;
        //printf("%02X ", data[k]);
      }
      else 
      {
        data[k] = 0;
        //printf("-- ");
      }
    }
    //printf("\n");
    encode_rs();
    
    // copy output data 
    for(k=0; k<(nn-kk); k++)
    {
      setNextNibble(bb[k]);
    }

    for(k=0; k<kk; k++)
    {
      setNextNibble(data[k]);
    }
    lenOutAvail += 15;
    
    //if (lenInPend == 1)

    
    if ( internalSet_hiLoState == 1)
    {
      if ( lenOutAvail/2 >= lenOutAbs/2)
      {
        retval+= 8;
      }
      else       
        retval+= 7;
    }
      else
      retval+= 8;
      
    if (lenOutAvail >= lenOutAbs)
      break;
  }
  return retval;
}


/** @brief decode one data block
 *  @note  a call rs_initDecoding must init. this method.
 *  @sa rs_initDecoding
 */
uint8_t decodeDataBlock( uint8_t len)
{
  uint32_t k, lenPay=0;

  for (k=0; k<nn; k++)
    recd[k] = getNextNibble();

  for (k=0; k<nn; k++)
    recd[k] = index_of[recd[k]] ;          /* put recd[i] into index form */
  // // !! CALL DECCODER HERE !! // //
  decode_rs();

  // copy output data 
  for(k=(nn-kk); k<(nn); k++)
  {
      setNextNibble(recd[k]);
      lenPay++;
  }
  
  return (uint8_t) lenPay/2;
}

///////////////////////////////////////////////////////////////////////
// whole telegramm en/decoding .. (old)

/**
 *  \brief splits data into nibbles and call the encoder for every block
 * 
 *  \param pointer to start of input uint8_t data array
 *  \param input length in byte 
 *  \param pointer to output buffer 
 *  \return number of bytes to transmit from ouput puffer
 ********/ 
uint8_t encodeDataString(uint8_t *pStart, uint8_t len, uint8_t *pOut)
{
  uint8_t nibbleBuffer[9];
  uint32_t i,k,rv=0;
  
  initGetNibble(pStart, 0);  // set internal vars for nibble get method
  initSetNibble(pOut, 0);
    
  for (i=0; i < len*2; i+=9)    // loop index i is nibble (4b) count
  {
    for (k=0; k< sizeof(nibbleBuffer); k++)
      data[k] = getNextNibble();

    // // !! CALL ENCCODER HERE !! // //
    //encoder_stub(nibbleBuffer);
    encode_rs();
    
    //printf("\n ");
    // copy output data 
    for(k=0; k<(nn-kk); k++)
      {
		  setNextNibble(bb[k]);
		  //printf("%02X ", bb[k]);
	  }
	  //printf("\n ");
    for(k=0; k<9; k++)
    {
      setNextNibble(data[k]);
      //printf("%02X ", data[k]);
  }
  //printf("\n");
  }
  
  return (uint8_t) (rv/2);
}

/** @brief decode data
 *  @note first (decoded) byte is interpreted as length 
 */
uint8_t decodeDataString(uint8_t *pStart, uint8_t len, uint8_t *pOut)
{
  //uint8_t nibbleBuffer[9];
  uint32_t i,k, lenPay=0;
  
  initGetNibble(pStart, 0);  // set internal vars for nibble get method
  initSetNibble(pOut, 0);
  
  for (i=0; i < 2*len; i+=15)    // loop index i is nibble (4b) count
  {
    for (k=0; k< nn; k++)
      recd[k] = getNextNibble();

    for (k=0; k<nn; k++)
      recd[k] = index_of[recd[k]] ;          /* put recd[i] into index form */
    // // !! CALL DECCODER HERE !! // //
    decode_rs();

    // copy output data 
    for(k=(nn-kk); k<(nn); k++)
    {
		    setNextNibble(recd[k]);
        lenPay++;
		  //printf("%02X ",recd[k]);
	  }
  }
  
  return (uint8_t) lenPay/2;

}

/** returns nibble wise data from uint8_t data vector */
  uint8_t getNextNibble(void)
{
  uint8_t t;
#ifdef START_WITH_MS_NIBBLE
  if (internalGet_hiLoState == 0)
  {
    t= ((*internalGet_pData)  & 0xF0) >> 4;
    internalGet_hiLoState = 1;

  } else
  {
    t= (*internalGet_pData)  & 0x0F;
    internalGet_hiLoState = 0; 
    internalGet_pData++;
  }
#else
  #error not implemented!
#endif
  
  return t;
}


void fillwithZeros(void)
{
  
}

/** returns nibble wise data from uint8_t data vector */
uint8_t setNextNibble(uint8_t v)
{
   v = v & 0x0F;
  
#ifdef START_WITH_MS_NIBBLE
  if (internalSet_hiLoState == 0)
  {
    *internalSet_pData =  v << 4;
    internalSet_hiLoState = 1;

  } else
  {
    (*internalSet_pData)  |= v;
    internalSet_hiLoState = 0; 
    internalSet_pData++;
  }
#else
  #error not implemented!
#endif
  
  return 0;
}

void initSetNibble(uint8_t *pStart, uint8_t hiLoState) 
{
  internalSet_pData =  pStart;
  internalSet_hiLoState = hiLoState;
}


void initGetNibble(uint8_t *pStart, uint8_t hiLoState) 
{
  internalGet_pData =  pStart;
  internalGet_hiLoState = hiLoState;
}

void initReedSolomon(void)
{
    generate_gf();
    gen_poly();
}

uint32_t calc_encodedLengthRS(uint32_t decodedLen)
{
  return ceil(ceil(decodedLen/4.5)*7.5);
}

uint32_t calc_encodedLengthShortcutedRS(uint32_t decodedLen)
{
  return 3*ceil(decodedLen/4.5) + decodedLen;
}

void test_rslen(void)
{
  volatile uint8_t i,len[149], len_rs[149], len_srs[149];
  
  for (i=0; i < sizeof(len); i++)
  {
    len[i] = i;
    len_rs[i] = calc_encodedLengthRS(len[i]);
    len_srs[i] = calc_encodedLengthShortcutedRS(len[i]);
  }
}

void reset_decodeStatistics(void)
{
  decodingErrStat_err = 0;
  decodingErrStat_syn_error = 0; 
}

void get_decodeStatistics(int *bErr, int *decodeFail)
{
  *bErr = decodingErrStat_err;
  *decodeFail = decodingErrStat_syn_error;
}

//#ifdef TEST___ 
//static uint8_t outputBuff[256];
//int main(void)
//{
//  uint8_t ob[100]={0},ib[100] ={0x1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31,0}; 
//  uint8_t len,i, ret ;
//  
//  generate_gf();
//  gen_poly();
//  
//  len = 5;
//  
//  printf("raw input data, len = %d\n", len); 
//  for (i=0; i< len;i++)
//    printf("%02X ", ib[i]); 
//      
//  // ENCODING
//  rs_initEncoding(&ib[0],len,&ob[0]);
//  ret = dataInAndEncode(len);
//  
//  printf("encoded data, len = %d\n", ret); 
//  for (i=0; i< ret;i++)
//    printf("%02X ", outputBuff[i]); 
//    
//  // ADD DUMMY FAILURES 
//  //outputBuff[6] ^= 0x3;
//  //printf("\nreceived data, len = %d\n", len); 
//  
//  for (i=0; i< len;i++)
//    //printf("%02X ", outputBuff[i]);
//    
//  // DECODING
//  len = decodeDataString(outputBuff, len, ob);

//  //printf("decoded data, len = %d\n", len); 
//  for (i=0; i< len;i++)
//    //printf("%02X ", ob[i]);
//  
//  return 0;
//}
//#endif

#ifdef TEST_SRS
static uint8_t outputBuff[256];
int main(void)
{
  uint8_t ob[100]={0},ib[100] ={0x1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31,0}; 
  uint8_t len,len_enc, i,u,ret,k,v;
  
  generate_gf();
  gen_poly();
  
  for (len =10; len<11; len++)
  {
      len_enc = (uint8_t) calc_encodedLengthShortcutedRS((uint32_t)len);
      
      printf("\n\n\nraw input data, len = %d\n", len); 
      for (i=0; i< len;i++)
        printf("%02X ", ib[i]); 
      printf("\nprecalc. length of enc. data = %d\n", len_enc);     
      // ENCODING
      rs_initEncoding(&ib[0],len,&outputBuff[0]);
      ret = 0;
      //while (ret < len_enc)
      for (i=0; i< len;i++)
        ret += dataInAndEncode(1);
      
      printf("\nencoded data, len = %d\n", ret);

      for (i=0; i< ret;i++)
        printf("%02X ", outputBuff[i]); 
       
      // ADD DUMMY FAILURES 
      outputBuff[5] ^= 0xF0;
      outputBuff[6] ^= 0xFF;
      outputBuff[11] ^= 0xf0;
      outputBuff[12] ^= 0x0F;
      outputBuff[20] ^= 0x0F;
      outputBuff[21] ^= 0x55;
      
      //printf("\nreceived data, len = %d\n", len); 
      
      //for (i=0; i< len;i++)
        //printf("%02X ", outputBuff[i]);
        
      // DECODING
      v = decodeDataString(outputBuff, len_enc, ob);

      printf("\ndecoded data, len=%d, bErr =%d, fail=%d\n", v, decodingErrStat_err, decodingErrStat_syn_error); 
      for (i=0; i< v;i++)
        printf("%02X ", ob[i]);
  }
  return 0;
}
#endif





void generate_gf()
/* generate GF(2**mm) from the irreducible polynomial p(X) in pp[0]..pp[mm]
   lookup tables:  index->polynomial form   alpha_to[] contains j=alpha**i;
                   polynomial form -> index form  index_of[j=alpha**i] = i
   alpha=2 is the primitive element of GF(2**mm)
*/
 {
   register int i, mask ;

  mask = 1 ;
  alpha_to[mm] = 0 ;
  for (i=0; i<mm; i++)
   { alpha_to[i] = mask ;
     index_of[alpha_to[i]] = i ;
     if (pp[i]!=0)
       alpha_to[mm] ^= mask ;
     mask <<= 1 ;
   }
  index_of[alpha_to[mm]] = mm ;
  mask >>= 1 ;
  for (i=mm+1; i<nn; i++)
   { if (alpha_to[i-1] >= mask)
        alpha_to[i] = alpha_to[mm] ^ ((alpha_to[i-1]^mask)<<1) ;
     else alpha_to[i] = alpha_to[i-1]<<1 ;
     index_of[alpha_to[i]] = i ;
   }
  index_of[0] = -1 ;
 }


void gen_poly()
/* Obtain the generator polynomial of the tt-error correcting, length
  nn=(2**mm -1) Reed Solomon code  from the product of (X+alpha**i), i=1..2*tt
*/
 {
   register int i,j ;

   gg[0] = 2 ;    /* primitive element alpha = 2  for GF(2**mm)  */
   gg[1] = 1 ;    /* g(x) = (X+alpha) initially */
   for (i=2; i<=nn-kk; i++)
    { gg[i] = 1 ;
      for (j=i-1; j>0; j--)
        if (gg[j] != 0)  gg[j] = gg[j-1]^ alpha_to[(index_of[gg[j]]+i)%nn] ;
        else gg[j] = gg[j-1] ;
      gg[0] = alpha_to[(index_of[gg[0]]+i)%nn] ;     /* gg[0] can never be zero */
    }
   /* convert gg[] to index form for quicker encoding */
   for (i=0; i<=nn-kk; i++)  gg[i] = index_of[gg[i]] ;
 }


void encode_rs()
/* take the string of symbols in data[i], i=0..(k-1) and encode systematically
   to produce 2*tt parity symbols in bb[0]..bb[2*tt-1]
   data[] is input and bb[] is output in polynomial form.
   Encoding is done by using a feedback shift register with appropriate
   connections specified by the elements of gg[], which was generated above.
   Codeword is   c(X) = data(X)*X**(nn-kk)+ b(X)          */
 {
   register int i,j ;
   int feedback ;

   for (i=0; i<nn-kk; i++)   bb[i] = 0 ;
   for (i=kk-1; i>=0; i--)
    {  feedback = index_of[data[i]^bb[nn-kk-1]] ;
       if (feedback != -1)
        { for (j=nn-kk-1; j>0; j--)
            if (gg[j] != -1)
              bb[j] = bb[j-1]^alpha_to[(gg[j]+feedback)%nn] ;
            else
              bb[j] = bb[j-1] ;
          bb[0] = alpha_to[(gg[0]+feedback)%nn] ;
        }
       else
        { for (j=nn-kk-1; j>0; j--)
            bb[j] = bb[j-1] ;
          bb[0] = 0 ;
        } ;
    } ;
 } ;



void decode_rs()
/* assume we have received bits grouped into mm-bit symbols in recd[i],
   i=0..(nn-1),  and recd[i] is index form (ie as powers of alpha).
   We first compute the 2*tt syndromes by substituting alpha**i into rec(X) and
   evaluating, storing the syndromes in s[i], i=1..2tt (leave s[0] zero) .
   Then we use the Berlekamp iteration to find the error location polynomial
   elp[i].   If the degree of the elp is >tt, we cannot correct all the errors
   and hence just put out the information symbols uncorrected. If the degree of
   elp is <=tt, we substitute alpha**i , i=1..n into the elp to get the roots,
   hence the inverse roots, the error location numbers. If the number of errors
   located does not equal the degree of the elp, we have more than tt errors
   and cannot correct them.  Otherwise, we then solve for the error value at
   the error location and correct the error.  The procedure is that found in
   Lin and Costello. For the cases where the number of errors is known to be too
   large to correct, the information symbols as received are output (the
   advantage of systematic encoding is that hopefully some of the information
   symbols will be okay and that if we are in luck, the errors are in the
   parity part of the transmitted codeword).  Of course, these insoluble cases
   can be returned as error flags to the calling routine if desired.   */
 {
   register int i,j,u,q ;
   int elp[nn-kk+2][nn-kk], d[nn-kk+2], l[nn-kk+2], u_lu[nn-kk+2], s[nn-kk+1] ;
   int count=0, syn_error=0, root[tt], loc[tt], z[tt+1], err[nn], reg[tt+1] ;

/* first form the syndromes */
   for (i=1; i<=nn-kk; i++)
    { s[i] = 0 ;
      for (j=0; j<nn; j++)
        if (recd[j]!=-1)
          s[i] ^= alpha_to[(recd[j]+i*j)%nn] ;      /* recd[j] in index form */
/* convert syndrome from polynomial form to index form  */
      if (s[i]!=0)  syn_error=1 ;        /* set flag if non-zero syndrome => error */
      s[i] = index_of[s[i]] ;
    } ;

   if (syn_error)       /* if errors, try and correct */
    {
/* compute the error location polynomial via the Berlekamp iterative algorithm,
   following the terminology of Lin and Costello :   d[u] is the 'mu'th
   discrepancy, where u='mu'+1 and 'mu' (the Greek letter!) is the step number
   ranging from -1 to 2*tt (see L&C),  l[u] is the
   degree of the elp at that step, and u_l[u] is the difference between the
   step number and the degree of the elp.
*/
/* initialise table entries */
      d[0] = 0 ;           /* index form */
      d[1] = s[1] ;        /* index form */
      elp[0][0] = 0 ;      /* index form */
      elp[1][0] = 1 ;      /* polynomial form */
      for (i=1; i<nn-kk; i++)
        { elp[0][i] = -1 ;   /* index form */
          elp[1][i] = 0 ;   /* polynomial form */
        }
      l[0] = 0 ;
      l[1] = 0 ;
      u_lu[0] = -1 ;
      u_lu[1] = 0 ;
      u = 0 ;

      do
      {
        u++ ;
        if (d[u]==-1)
          { l[u+1] = l[u] ;
            for (i=0; i<=l[u]; i++)
             {  elp[u+1][i] = elp[u][i] ;
                elp[u][i] = index_of[elp[u][i]] ;
             }
          }
        else
/* search for words with greatest u_lu[q] for which d[q]!=0 */
          { q = u-1 ;
            while ((d[q]==-1) && (q>0)) q-- ;
/* have found first non-zero d[q]  */
            if (q>0)
             { j=q ;
               do
               { j-- ;
                 if ((d[j]!=-1) && (u_lu[q]<u_lu[j]))
                   q = j ;
               }while (j>0) ;
             } ;

/* have now found q such that d[u]!=0 and u_lu[q] is maximum */
/* store degree of new elp polynomial */
            if (l[u]>l[q]+u-q)  l[u+1] = l[u] ;
            else  l[u+1] = l[q]+u-q ;

/* form new elp(x) */
            for (i=0; i<nn-kk; i++)    elp[u+1][i] = 0 ;
            for (i=0; i<=l[q]; i++)
              if (elp[q][i]!=-1)
                elp[u+1][i+u-q] = alpha_to[(d[u]+nn-d[q]+elp[q][i])%nn] ;
            for (i=0; i<=l[u]; i++)
              { elp[u+1][i] ^= elp[u][i] ;
                elp[u][i] = index_of[elp[u][i]] ;  /*convert old elp value to index*/
              }
          }
        u_lu[u+1] = u-l[u+1] ;

/* form (u+1)th discrepancy */
        if (u<nn-kk)    /* no discrepancy computed on last iteration */
          {
            if (s[u+1]!=-1)
                   d[u+1] = alpha_to[s[u+1]] ;
            else
              d[u+1] = 0 ;
            for (i=1; i<=l[u+1]; i++)
              if ((s[u+1-i]!=-1) && (elp[u+1][i]!=0))
                d[u+1] ^= alpha_to[(s[u+1-i]+index_of[elp[u+1][i]])%nn] ;
            d[u+1] = index_of[d[u+1]] ;    /* put d[u+1] into index form */
          }
      } while ((u<nn-kk) && (l[u+1]<=tt)) ;

      u++ ;
      if (l[u]<=tt)         /* can correct error */
       {
/* put elp into index form */
         for (i=0; i<=l[u]; i++)   elp[u][i] = index_of[elp[u][i]] ;

/* find roots of the error location polynomial */
         for (i=1; i<=l[u]; i++)
           reg[i] = elp[u][i] ;
         count = 0 ;
         for (i=1; i<=nn; i++)
          {  q = 1 ;
             for (j=1; j<=l[u]; j++)
              if (reg[j]!=-1)
                { reg[j] = (reg[j]+j)%nn ;
                  q ^= alpha_to[reg[j]] ;
                } ;
             if (!q)        /* store root and error location number indices */
              { root[count] = i;
                loc[count] = nn-i ;
                count++ ;
              };
          } ;
         if (count==l[u])    /* no. roots = degree of elp hence <= tt errors */
          {
/* form polynomial z(x) */
           for (i=1; i<=l[u]; i++)        /* Z[0] = 1 always - do not need */
            { if ((s[i]!=-1) && (elp[u][i]!=-1))
                 z[i] = alpha_to[s[i]] ^ alpha_to[elp[u][i]] ;
              else if ((s[i]!=-1) && (elp[u][i]==-1))
                      z[i] = alpha_to[s[i]] ;
                   else if ((s[i]==-1) && (elp[u][i]!=-1))
                          z[i] = alpha_to[elp[u][i]] ;
                        else
                          z[i] = 0 ;
              for (j=1; j<i; j++)
                if ((s[j]!=-1) && (elp[u][i-j]!=-1))
                   z[i] ^= alpha_to[(elp[u][i-j] + s[j])%nn] ;
              z[i] = index_of[z[i]] ;         /* put into index form */
            } ;

  /* evaluate errors at locations given by error location numbers loc[i] */
           for (i=0; i<nn; i++)
             { err[i] = 0 ;
               if (recd[i]!=-1)        /* convert recd[] to polynomial form */
                 recd[i] = alpha_to[recd[i]] ;
               else  recd[i] = 0 ;
             }
           for (i=0; i<l[u]; i++)    /* compute numerator of error term first */
            { err[loc[i]] = 1;       /* accounts for z[0] */
              for (j=1; j<=l[u]; j++)
                if (z[j]!=-1)
                  err[loc[i]] ^= alpha_to[(z[j]+j*root[i])%nn] ;
              if (err[loc[i]]!=0)
               { err[loc[i]] = index_of[err[loc[i]]] ;
                 q = 0 ;     /* form denominator of error term */
                 for (j=0; j<l[u]; j++)
                   if (j!=i)
                     q += index_of[1^alpha_to[(loc[j]+root[i])%nn]] ;
                 q = q % nn ;
                 err[loc[i]] = alpha_to[(err[loc[i]]-q+nn)%nn] ;
                 recd[loc[i]] ^= err[loc[i]] ;  /*recd[i] must be in polynomial form */
                   
                 for(uint8_t qq=0; qq<8; qq++)  // bit error counter 
                    if ( err[loc[i]] & (1 << qq))
                      decodingErrStat_err++;
               }
            }
          }
         else    /* no. roots != degree of elp => >tt errors and cannot solve */
         {
           decodingErrStat_syn_error++;
           for (i=0; i<nn; i++)        /* could return error flag if desired */
               if (recd[i]!=-1)        /* convert recd[] to polynomial form */
                 recd[i] = alpha_to[recd[i]] ;
               else  recd[i] = 0 ;     /* just output received codeword as is */
         }
       }
     else         /* elp has degree has degree >tt hence cannot solve */
     {
       decodingErrStat_syn_error++;
       for (i=0; i<nn; i++)       /* could return error flag if desired */
          if (recd[i]!=-1)        /* convert recd[] to polynomial form */
            recd[i] = alpha_to[recd[i]] ;
          else  recd[i] = 0 ;     /* just output received codeword as is */
     }
    }
   else       /* no non-zero syndromes => no errors: output received codeword */
    for (i=0; i<nn; i++)
       if (recd[i]!=-1)        /* convert recd[] to polynomial form */
         recd[i] = alpha_to[recd[i]] ;
       else  recd[i] = 0 ;
 }


 
 
// Test world 
