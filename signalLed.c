/** @file signalLed.c 
  *   LED Signaling implementation for E271 NB IoT. 
  *   uses Systick Timer source clock to output LED signaling in 
  *   modes: 
  *
  *   use ledStausInit() to set the port pin to open drain output mode
  *
  *   use function call ledStatusSet(uint32_t state)
  *     with state:
  *
  *  LED_OFF           
  *  LED_ON            
  *  LED_BLINKSLOW     
  *  LED_BLINKFAST     
  *  LED_BLINKFASTFAST 
  *  LED_TOGGLE 
  *
  *  additionally there is a coded blinking to show states etc: 
  *       void ledStatusSetBlinkcode(uint8_t code)
  *
  *                             _   _       _   _
  *  example code =2 => LED   _| |_| |_____| |_| |_____..
  *                             _   _   _   _       _   _   _   _
  *          code =4 => LED   _| |_| |_| |_| |_____| |_| |_| |_| |___.. 
  *                           
  *                           0.2s/0.2ms coding+ 4x200ms idle(off)
  */ 

#include "signalLed.h"

volatile uint32_t usrTickCntr=0;
volatile bool usrLedBlinkMode=false, usrLedBlueBlinkMode=false;
uint8_t usrLedBlinkCoded=0, usrLedBlueBlinkCoded=0;
volatile uint8_t userLed_codeInternalState=0, userLedBlue_codeInternalState=0;
volatile uint8_t userLed_codeInternalNumbOfStates=0, userLedBlue_codeInternalNumbOfStates=0;
uint32_t usrLedDivider=1, usrLedBlueDivider;

void ledSignalingRoutine(void)
{
  if (usrLedBlinkMode == true)
  {
    if (usrLedBlinkCoded > 0)
    { 
      if (userLed_codeInternalState < (usrLedBlinkCoded*2))
        HAL_GPIO_TogglePin(OPERATION_STATUS_GPIO_Port, OPERATION_STATUS_Pin);
      
      userLed_codeInternalState++;
      
      if (userLed_codeInternalState >= userLed_codeInternalNumbOfStates)
        userLed_codeInternalState = 0;
    } 
    else 
      HAL_GPIO_TogglePin(OPERATION_STATUS_GPIO_Port, OPERATION_STATUS_Pin); 
  }
}

void ledBlueSignalingRoutine(void)
{
  if (usrLedBlueBlinkMode == true)
  {
    if (usrLedBlueBlinkCoded > 0)
    { 
      if (userLedBlue_codeInternalState < (usrLedBlueBlinkCoded*2))
        HAL_GPIO_TogglePin(LED_BLUE_GPIO_Port, LED_BLUE_Pin);
      
      userLedBlue_codeInternalState++;
      
      if (userLedBlue_codeInternalState >= userLedBlue_codeInternalNumbOfStates)
        userLedBlue_codeInternalState = 0;
    } 
    else 
      HAL_GPIO_TogglePin(LED_BLUE_GPIO_Port, LED_BLUE_Pin); 
  }
}

/** @ service routine from sysclk timer to driver led signals */ 
void HAL_SYSTICK_Callback(void)
{
  usrTickCntr++;
  
  if ((usrTickCntr % (10*usrLedDivider) ) == 0)
  {
    ledSignalingRoutine();
  } 
  
  if ((usrTickCntr % (10*usrLedBlueDivider) ) == 0)
  {
    ledBlueSignalingRoutine();
  }
}

/** @brief set different signals on LED (green) 
  * @param please use predefined symbols LED_...
  */
void ledStatusSet(uint32_t state)
{
  switch (state)
  {
    default: 
    case LED_OFF:
      usrLedBlinkMode = false;
      HAL_GPIO_WritePin (OPERATION_STATUS_GPIO_Port, OPERATION_STATUS_Pin, GPIO_PIN_SET);
      break;
    case LED_ON:
      usrLedBlinkMode = false;
      HAL_GPIO_WritePin (OPERATION_STATUS_GPIO_Port, OPERATION_STATUS_Pin, GPIO_PIN_RESET);
      break;
   case LED_BLINKSLOW:
      usrLedDivider = 100;
      usrLedBlinkMode = true;
      break;  
    case LED_BLINKFAST:
      usrLedBlinkMode = true;
      usrLedDivider = 50;
      break;
    case LED_BLINKFAST2:
      usrLedBlinkMode = true;
      usrLedDivider = 10;
      break;
    case LED_BLINKFAST3:
      usrLedBlinkMode = true;
      usrLedDivider = 5;
      break;
    case LED_BLINKFAST4:
      usrLedBlinkMode = true;
      usrLedDivider = 1;
      break;
    case LED_BLINKCODED:
      usrLedBlinkMode = true;
      usrLedDivider = 20;
      break;
   case LED_TOGGLE:
      usrLedBlinkMode = false;
      HAL_GPIO_TogglePin(OPERATION_STATUS_GPIO_Port, OPERATION_STATUS_Pin);
      break;
  }  
  
  if ( state != LED_BLINKCODED)
    usrLedBlinkCoded = 0;
}

/** @brief set blink code for special blink mode LED_BLINKCODED.
  *        blinking will be repeated until other mode is set.  
  * 
  *  @param code:  results *code* pulses per cycle on the LED
  *  
  *  @param implizit actives LED blinking too, there is no need to call ledStatusSet separatly 
  *                             _   _       _   _
  *  example code =2 => LED   _| |_| |_____| |_| |_____..
  *                             _   _   _   _       _   _   _   _
  *          code =4 => LED   _| |_| |_| |_| |_____| |_| |_| |_| |___.. 
  *                           
  *                           0.2s/0.2ms coding+ 4x200ms idle(off)
  *                                
  */
void ledStatusSetBlinkcode(uint8_t code)
{
  ledStatusSet(LED_OFF);
  ledStatusSet(LED_BLINKCODED);
  usrLedBlinkCoded = code;
  userLed_codeInternalNumbOfStates = code*2+4;
  userLed_codeInternalState=0;

  if (code == 0) 
    usrLedBlinkMode =false;
}

void ledBlueSetBlinkcode(uint8_t code)
{
  ledBlueSet(LED_OFF);
  ledBlueSet(LED_BLINKCODED);
  usrLedBlueBlinkCoded = code;
  userLedBlue_codeInternalNumbOfStates = code*2+4;
  userLedBlue_codeInternalState=0;

  if (code == 0) 
    usrLedBlueBlinkMode =false;
}


/** @brief init both leds NETLIGHT (blue) and STATUS (green)
  *        and apply test impuls (200ms) on both.   
  */
void ledStatusInit(void)
{
  ledBlueSet(LED_ON);
  ledStatusSet(LED_ON);
  HAL_Delay(200); 
  ledBlueSet(LED_OFF);
  ledStatusSet(LED_OFF);
}


/** @brief set different signals on LED (blue) 
  * @param please use predefined symbols LED_...
  */
void ledBlueSet(uint32_t state)
{
  switch (state)
  {
    default: 
    case LED_OFF:
      usrLedBlueBlinkMode = false;
      HAL_GPIO_WritePin(LED_BLUE_GPIO_Port, LED_BLUE_Pin, GPIO_PIN_RESET);
      break;
    case LED_ON:
      usrLedBlueBlinkMode = false;
      HAL_GPIO_WritePin(LED_BLUE_GPIO_Port, LED_BLUE_Pin, GPIO_PIN_SET);
      break;
   case LED_BLINKSLOW:
      usrLedBlueDivider = 100;
      usrLedBlueBlinkMode = true;
      break;  
    case LED_BLINKFAST:
      usrLedBlueBlinkMode = true;
      usrLedBlueDivider = 50;
      break;
    case LED_BLINKFAST2:
      usrLedBlueBlinkMode = true;
      usrLedBlueDivider = 10;
      break;
    case LED_BLINKFAST3:
      usrLedBlueBlinkMode = true;
      usrLedBlueDivider = 5;
      break;
    case LED_BLINKFAST4:
      usrLedBlueBlinkMode = true;
      usrLedBlueDivider = 1;
      break;
    case LED_BLINKCODED:
      usrLedBlueBlinkMode = true;
      usrLedBlueDivider = 20;
      break;
   case LED_TOGGLE:
      usrLedBlueBlinkMode = false;
      HAL_GPIO_TogglePin(LED_BLUE_GPIO_Port, LED_BLUE_Pin);
      break;
  }  
  
  if ( state != LED_BLINKCODED)
    usrLedBlueBlinkCoded = 0;
}