/*!
 * \file circStrBufferDual.h
 * \brief general purpose circular buffer with 
 *        fixed byte-length string handling and double instance
 *        of two individual buffer, for in/out etc. 
 *
 * \author Droessler, Tobias
 * \date Nov/2013
 ************************************************/ 

#include "circStrBufferDual.h"

struct Buffer {
  uint8_t data[2][BUFFER_SIZE];     /**< data field */
  uint16_t read[2]; 				/**< read index  */
  uint16_t write[2];				/**< write index */
  uint16_t currLevel[2];            /**< fill level in Byte */
} buffer = {  .read = {0}, .write = {0}, .currLevel = {0}}; //needs C99 extentions enabled. 
 
/************************************************************************//**
 * \brief	Put byte into buffer.
 * \param 	Pointer to first Byte
 * @param   buffInd - Buffer instance. 
 * \return	SUCCESS or FAIL 
 *			
 ***************************************************************************/
static uint8_t buffIn(uint8_t buffInd, uint8_t byte)
{
  uint16_t next = ((buffer.write[buffInd] + 1) & BUFFER_MASK);
  if (buffer.read[buffInd] == next)
    return FAIL;
  buffer.data[buffInd][buffer.write[buffInd]]  = byte;
  buffer.currLevel[buffInd]++;
  // buffer.data[buffer.write & BUFFER_MASK] = byte; // absolut Sicher
  buffer.write[buffInd] = next;
  return SUCCESS;
}


/************************************************************************//**
 * \brief	Get next byte out of buffer.
 * \param 	*pByte - destination pointer
 * @param   buffInd - Buffer instance. 
 * \return	SUCCESS or FAIL 
 *			
 **************************************************************************/ 
static uint8_t buffOut(uint8_t buffInd, uint8_t *pByte)
{
  if (buffer.read[buffInd] == buffer.write[buffInd])
    return FAIL;
  *pByte = buffer.data[buffInd][buffer.read[buffInd]];
  buffer.read[buffInd] = (buffer.read[buffInd]+ 1) & BUFFER_MASK;
  buffer.currLevel[buffInd]--;
  return SUCCESS;
}


/**************************************************************************//**
 * @brief   Returns current byte count inside buffer. 
 * @param   buffInd - Buffer instance. 
 * @return  Total bytes available in buffer. 
 *****************************************************************************/
uint16_t buffGetLevel(uint8_t buffInd)
{
	return buffer.currLevel[buffInd];
}



/**************************************************************************//**
 * @brief   Adaption to input whole strings with fixed length BUFFER_STR_SIZE.
 * @param   p - Pointer to first data byte.
 * @param   buffInd - Buffer instance. 
 * @return  SUCCESS or FAIL 
 *****************************************************************************/
uint8_t buffStrIn(uint8_t buffInd, uint8_t *p)
{
  uint8_t i,ret;
  ret = SUCCESS;
  for(i=0; i< BUFFER_STR_SIZE; i++)
  {
    if ( buffIn(buffInd,*(p++)) == FAIL )
      {
        ret = FAIL;
        break;
      }
  }
  return ret;
}


/**************************************************************************//**
 * @brief   Adaption to Output whole strings with fixed length of
 *          BUFFER_STR_SIZE.
 * @param   p - Pointer to first byte of output destination.
 * @param   buffInd - Buffer instance. 
 * @return  SUCCESS or FAIL 
 *****************************************************************************/
uint8_t buffStrOut(uint8_t buffInd, uint8_t *p)
{
  uint8_t i,ret;
  ret = SUCCESS;
  for(i=0; i< BUFFER_STR_SIZE; i++)
  {
    if ( buffOut(buffInd, p++) == FAIL )
    {
      ret = FAIL;
      break;
    }
  }
  return ret;
}



#define TEST_FIXED_STR_BUFFER 
#ifdef TEST_FIXED_STR_BUFFER 


#include <stdio.h>

int main(void)
{

  uint8_t ret, i,tvr[8],tv[8] = {0x11, 0x22 , 0x33, 0x44, 0x55, 0x66, 0x77, 0x88};
  uint8_t tv_a[8] = {0x1, 0x2 , 0x3, 0x4, 0x5, 0x6, 0x7, 0x8};



  for (i=0;i<(BUFFER_SIZE/BUFFER_STR_SIZE-1); i++)
  {

    if (buffStrIn(0,tv) == FAIL)
      printf("Err: BuffStrIn FAILs, Buffer should be not full, i=%d\n",i);
    if (buffStrIn(1,tv_a) == FAIL)
      printf("Err: BuffStrIn FAILs, Buffer should be not full, i=%d\n",i);


  }

  if (buffStrIn(0,tv) ==SUCCESS )
      printf("Err: Buffer full and no FAIL returnd\n");


  if (buffStrIn(1,tv_a) ==SUCCESS )
      printf("Err: Buffer full and no FAIL returnd\n");
  
  for (i=0;i<(BUFFER_SIZE/BUFFER_STR_SIZE-1);i++)
  {
    uint8_t j;

    if (buffStrOut(0,tvr) == FAIL)
      printf("Err: BuffStrOut FAILs, but data should be stored,i=%d\n", i);

    for (j=0;j<8; j++)
      if(tv[j] != tvr[j])
        printf("Err: Data not identcial outputted, j=%d\n", j);
   
    if (buffStrOut(1,tvr) == FAIL)
      printf("Err: BuffStrOut FAILs, but data should be stored,i=%d\n", i);

    for (j=0;j<8; j++)
      if(tv_a[j] != tvr[j])
        printf("Err: Data not identcial outputted, j=%d\n", j);


  } 
  printf("Test done\n");
}

#endif

