/*!
 * \file circStrBufferDual.h
 * \brief general purpose circular buffer with 
 *        fixed byte-length string handling and double instance
 *        of two individual buffer, for in/out etc. 
 *
 * \author Droessler, Tobias
 * \date Nov/2013
 ************************************************/ 

#include <stdint.h>

 /* == DEFINEs == */
#define BUFFER_STR_SIZE      	(8)      ///< fixed count of bytes per element          

#define BUFFER_SIZE          	(64) 	 //  2^n (8, 16, 32, 64 ...)
#define BUFFER_MASK (BUFFER_SIZE-1)      //	 

#define BUFFER_IN				(0)	     // buffer instance for in data 
#define BUFFER_OUT				(1)      // buffer instance for out data 


#define SUCCESS 	   			(0)
#define FAIL		  			((uint8_t) -1 )

/* == PROTOTYPEs == */



/**************************************************************************//**
 * @brief   Returns current byte count inside buffer. 
 * @param   buffInd - Buffer instance. 
 * @return  Total bytes available in buffer. 
 *****************************************************************************/
uint16_t buffGetLevel(uint8_t buffInd);


/**************************************************************************//**
 * @brief   Adaption to input whole strings with fixed length BUFFER_STR_SIZE.
 * @param   p - Pointer to first data byte.
 * @param   buffInd - Buffer instance. 
 * @return  SUCCESS or FAIL 
 *****************************************************************************/
uint8_t buffStrIn(uint8_t buffInd, uint8_t *p);


/**************************************************************************//**
 * @brief   Adaption to Output whole strings with fixed length of
 *          BUFFER_STR_SIZE.
 * @param   p - Pointer to first byte of output destination.
 * @param   buffInd - Buffer instance. 
 * @return  SUCCESS or FAIL 
 *****************************************************************************/
uint8_t buffStrOut(uint8_t buffInd, uint8_t *p);

/* == INTERNAL PROTOTYPES == */
/************************************************************************//**
 * \brief	Put byte into buffer.
 * \param 	Pointer to first Byte
 * @param   buffInd - Buffer instance. 
 * \return	SUCCESS or FAIL 
 *			
 ***************************************************************************/
static uint8_t buffIn(uint8_t buffInd, uint8_t byte);

/************************************************************************//**
 * \brief	Get next byte out of buffer.
 * \param 	*pByte - destination pointer
 * @param   buffInd - Buffer instance. 
 * \return	SUCCESS or FAIL 
 *			
 **************************************************************************/ 
static uint8_t buffOut(uint8_t buffInd, uint8_t *pByte);


