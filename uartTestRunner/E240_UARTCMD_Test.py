import sys
import serial
import time
import datetime
import subprocess
import binascii
from threading import Timer

# open interface (UART over USB)
# COM Port zum Optokopf: folgende Zeile fuer Win, uebernaechste Z. Cygwin 
interface = serial.Serial(sys.argv[2], baudrate=57600, timeout=0.2, parity='N' );

print "opened port " + interface.name

def xorChkSumStr(a):
    aL= list(a)
    chsum= 0x53;
    for i in aL:
        chsum = chsum ^ ord(i)
    return chr(chsum)

def xorChkSum(a):
    chsum= 0x53;
    for i in a:
        chsum = chsum ^ i
    return chsum
    
def getTestInput(fd):
	string = fd.readline()

	if string != "":
		if string.find("//") > -1:
			comment = string.split("//")[1]
			comment = comment.split("\r\n")[0]
			string = string.split("//")[0]
		else:
			comment = ""
		string = string.replace(" ","")
		#print string
		inp = string.split("=")[0]
		should = string.split("=")[1]
		inp = inp.strip()
		should =should.strip()
#		print inp; print should;
		inp = binascii.a2b_hex(inp)
		should = binascii.a2b_hex(should)
	else:
		inp = should = comment ="";	
	return [inp, should, comment]
	
	
def txDataList(d):
	d_str = "".join([chr(x) for x in d])
	interface.write(d_str);
	
def txDataStr(d):
	interface.write(d);
	
def getRxData():
	retv = "".join(interface.readlines())
	return (retv)   

def printhex(a):
        print ''.join('%02x'%i for i in a)
        
def convertHexStr(s):
	return " ".join("{:02x}".format(ord(c)) for c in s)
	
# start testsender application 
interface.flushInput()


print "test run";
# open input file/stream
try: 
    sys.argv[1]
except:
    print "no test file found"
else:
    print "open input file " + sys.argv[1]
    f = open(sys.argv[1],'r')
    
if len(sys.argv) > 3:
	proto = open(sys.argv[3], 'w')
	proto.write( "Test date/time: %s\r\n" % datetime.datetime.now())
	proto.write("Line\tState\tCmdData,  DisiredData, AnsData,  Comment\r\n") 
linecount = 0
while True:
	myTestInput = getTestInput(f);
	if myTestInput[0] != "":
		linecount += 1
		#print(myTestInput); 
		myTestInput[0] = myTestInput[0] +xorChkSumStr(myTestInput[0])
		#print(myTestInput[0]);
		myTestInput[1] = myTestInput[1] +xorChkSumStr(myTestInput[1]) 
		txDataStr(myTestInput[0]);
		res = getRxData();

		if (res == myTestInput[1]):
			print "OK"
			if len(sys.argv) > 3:
				proto.write("%d\t%s\t%s, %s, %s, %s\r\n" % (linecount, "OK", convertHexStr(myTestInput[0]), 
																 convertHexStr(myTestInput[1]), 
																 convertHexStr(res), 
																 myTestInput[2] )) 
		else:
			print "NOK: Line %d, Test comment -> %s" % (linecount, myTestInput[2])
			print "  send:    " + convertHexStr(myTestInput[0])
			print "  desired: " + convertHexStr(myTestInput[1])
			print "  get:     " + convertHexStr(res)
			if len(sys.argv) > 3:
				proto.write("%d\t%s\t%s, %s, %s, %s\r\n" % (linecount, "NOK", convertHexStr(myTestInput[0]), \
																 convertHexStr(myTestInput[1]), \
																 convertHexStr(res), \
																 myTestInput[2] ))  
	else:
		break;
		print "done.." 
	#time.sleep(1)

if len(sys.argv) > 3:
	proto.close()
f.close()
interface.close()
