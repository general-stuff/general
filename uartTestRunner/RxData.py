import serial 
import struct
import sys


def dummyIndata():
	return '\xAC\xFF\xFF\x33\x44\x01\xf3\x03\x02'


def formatData(s):
	d= struct.unpack(">Ibbb", s[1:8])
	#print hex(d[0]), "     " , hex(d[1]) , d[2] , d[3]
	ascii = "".join("%8X %2X        %2d    %2d" % d) 
	return ascii
	
	
com =  serial.Serial(sys.argv[1], baudrate=57600, timeout=0.5, parity='N' );
log =  open("logfile.txt", 'w'); 
print     "==UID===  Data     RSSI    Subtelegrams" 
log.write("==UID===  Data     RSSI    Subtelegrams\r\n")
# print formatData(dummyIndata())
try:
    while True:
	if com.inWaiting() > 8:
		outline = formatData(com.read(9))
		print outline
		log.write(outline + '\r\n')
except KeyboardInterrupt:
    pass
    
com.close()
log.close()


