
#include <string.h>
#include <stdint.h>
#include <stdbool.h>
#include <stdlib.h>

#define ENTRIES 				16
#define DATALENGTH_PER_ENTRY 	160


typedef struct
{
	uint8_t id;
	uint8_t active; 
	uint8_t data[DATALENGTH_PER_ENTRY];
} type_id_data;




void initDatabase(void);
void setData(uint8_t id, uint8_t *pData);
uint8_t* getData(uint8_t id);
