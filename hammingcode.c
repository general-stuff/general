/**
 *       @file  hammingcode.c
 *      @brief  Hamming Encoding/Decoding (7,4) => Codes 4 bits of payload into 7 bits
 *              codeword for transmission etc.i
 *              Features: One biterror can be corrected !  
 *
 *
 *     @author  Tobias Drößler (TDR), droessler.tobias@ik-elektronik.com
 *
 *   @internal
 *     Created  09.08.2013
 *    Compiler  Keil ARMCC 
 *     Company  IK Elektronik
 *
 * =============================================================================
 */


#include <stdio.h>
#include <stdint.h>
#include "hammingcode.h"

/*==== local prototypes ====*/
uint8_t parityCalc(uint8_t pay);

/*==== function bodys ====*/

/**************************************************************************//**
 * @brief   encode Hamming (7,4) - 7bit codeword, 4bit payload 
 * @param   pay - four LSB bits are used, MSB nibble is ignored
 * @return  codeword (7bit)
 * @retval  ---------------------------------
 *          | |   parity   |     pay        |
 *          ---------------------------------
 *           7 6          4 3              0
 *****************************************************************************/
uint8_t encode_hamming(uint8_t pay)
{
	uint8_t p,ret= 0;

	p = parityCalc(pay);
	ret =  (p << 4) | (pay & 0x0F);
	return ret;
}

/**************************************************************************//**
 * @brief   decode Hamming codeword
 * @param   7 bit  codeword, MSB ignored
 * @return  payload, 4 bit least sign. nibble
 *****************************************************************************/
uint8_t decode_hamming(uint8_t cw)
{
	uint8_t p_rec, p_calc;

	p_rec = (cw & 0x70) >> 4;
	p_calc = parityCalc(cw & 0x0F);

	if (p_rec == p_calc)
		return cw & 0x0F;
    else
    {
        uint8_t error_bit, mask=0; 

        error_bit = p_rec ^ p_calc;
        
        if (error_bit  == 3)
            mask = 0x1;

        if (error_bit > 4)
            mask = 0x1 << (error_bit-4);

        return ((cw ^ mask)  & 0x0F);
    }
}


/**************************************************************************//**
 * @brief   Parity bit calculation.
 * @param   payload (least sign. nibble used!)
 * @return  parity bits  (p3..p1) 
 *****************************************************************************/
uint8_t parityCalc(uint8_t pay)
{
	uint8_t temp[3] = {0,0,0};
	
	if (pay & 0x01)
	{
		temp[0] = 0x3;
	}
	if (pay & 0x02)
	{
		temp[0] |= 0x4;
	   	temp[1]  = 0x1;
	}
	if (pay & 0x04)
	{
		temp[1] |= 0x6;
	}
	if (pay & 0x08)
	{
		temp[2] = 0x7;
	}
	return ( temp[0] ^ temp[1] ^ temp[2] );
}

// only for Test //
void main(void)
{
	uint8_t ham,out,result,i; // 0b0110;

	result = 1;

	for (i=0; i<=0xF; i++)
	{
		ham = encode_hamming(i);
		out = decode_hamming(ham);
		printf("in:%X =>  ham:%X => out:%X\n",i, ham, out);
		if (i != out)
		{
			result = 0;
			break;
		}
	}


	if (result)
		printf("Test successful done.\n");
	else
		printf("Test failed at input=%d.\n",i);

    printf("Test one bit errors..\n");
    result = 1;
    
    printf("in:%X =>  ham:%X ==>%X=> out:%X\n",0x02, encode_hamming(0x02), encode_hamming(0x02)^1, decode_hamming(encode_hamming(0x02)^1));
    if ( decode_hamming (encode_hamming (0x02)^0x01) != 0x02 )
       result = 0;

    printf("in:%X =>  ham:%X ==>%X=> out:%X\n",0x05, encode_hamming(0x05), encode_hamming(0x05)^4, decode_hamming(encode_hamming(0x05)^4));
    if ( decode_hamming (encode_hamming (0x05)^ 0x04) != 0x05 )
       result = 0;

    printf("in:%X =>  ham:%X ==>%X=> out:%X\n",0x0F, encode_hamming(0x0F), encode_hamming(0x0F)^2, decode_hamming(encode_hamming(0x0F)^2));
    if ( decode_hamming (encode_hamming (0x0F)^0x02) != 0x0F )
       result = 0;
 
    printf("in:%X =>  ham:%X ==>%X=> out:%X\n",0x07, encode_hamming(0x07), encode_hamming(0x07)^0x20, decode_hamming(encode_hamming(0x07)^0x20));
    if ( decode_hamming (encode_hamming (0x0F)^0x02) != 0x0F )
       result = 0;  	

    if (result)
		printf("Test successful done.\n");
	else
		printf("Test failed.");

    printf("Test two bit errors..\n");
    result = 1;
 
    printf("in:%X =>  ham:%X ==>%X=> out:%X\n",0x0F, encode_hamming(0x0F), encode_hamming(0x0F)^6, decode_hamming(encode_hamming(0x0F)^6));
    printf("in:%X =>  ham:%X ==>%X=> out:%X\n",0x0F, encode_hamming(0x0F), encode_hamming(0x0F)^7, decode_hamming(encode_hamming(0x0F)^7));
    //if ( decode_hamming (encode_hamming (0x0F)^0x06) != 0x0F )
 
}
