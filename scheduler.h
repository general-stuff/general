

#ifndef SCHEDULER_H
#define SCHEDULER_H



/* TASK Definitionen */
/* LSB: höchste Prio
 * MSB: genringste Prio
 *
 * nur ein Bit darf gesetzt sein!! 
 **/

#define TASK_EWARN		0x01
#define TASK_TIME		0x02
#define TASK_MIDNIGHT	0x80


/* PROTOTYPEs */

void scheduler_roundRobin(void);
void scheduler_hardPrio(void);
uint8_t scheduler_setTask(uint32_t taskToSet);


#endif /*SCHEDULER_H*/
