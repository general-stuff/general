/**!
 *       @file  enocean_switch_coder.c
 *      @brief  
 *
 *
 *     @author Tobias Drößler, IK Elektronik GmbH 
 *
 *   @internal
 *     Created  28.10.2014
 *    Compiler  gcc/g++
 *
 * This source code is released for free distribution under the terms of the
 * GNU General Public License as published by the Free Software Foundation.
 * =====================================================================================
 */



#include <stdint.h>

void eo_encode(uint8_t *id_ptr, uint8_t rorg, uint8_t data, uint8_t *destBuff );

/*
typedef struct {
	uint8_t preamb;
	


} telegram_t; */
