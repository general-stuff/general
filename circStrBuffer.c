/*!
 * \file circStrBuffer.c 
 * \brief general purpose circular buffer with 
 *        string handling. 
 *
 * \author Droessler, Tobias
 * \date 30.04.2013
 *
 *  Dec./2017: change for var. length strings 
 ************************************************/ 
#include <string.h>
#include "circStrBuffer.h"

struct Buffer {
  uint8_t data[BUFFER_SIZE];  /**< data field */
  uint16_t read; 				/**< read index  */
  uint16_t write;				/**< write index */
  uint16_t currLevel;
} buffer = {  .read = 0, .write = 0, .currLevel = 0}; //needs C99 extentions enabled. 
 
/************************************************************************//**
 * \brief	Put byte into buffer.
 * \param 	Pointer to first Byte
 * \return	SUCCESS or FAIL 
 *			
 ***************************************************************************/
static uint8_t buffIn(uint8_t byte)
{
  uint16_t next = ((buffer.write + 1) & BUFFER_MASK);
  if (buffer.read == next)
    return FAIL;
  buffer.data[buffer.write] = byte;
  buffer.currLevel++;
  // buffer.data[buffer.write & BUFFER_MASK] = byte; // absolut Sicher
  buffer.write = next;
  return SUCCESS;
}

/************************************************************************//**
 * \brief	Get next byte out of buffer.
 * \param 	*pByte - destination pointer
 * \return	SUCCESS or FAIL 
 *			
 **************************************************************************/ 
uint8_t buffOut(uint8_t *pByte)
{
  if (buffer.read == buffer.write)
    return FAIL;
  *pByte = buffer.data[buffer.read];
  buffer.read = (buffer.read+1) & BUFFER_MASK;
  buffer.currLevel--;
  return SUCCESS;
}

/**************************************************************************//**
 * @brief   Returns current byte count inside buffer. 
 * @return  Total bytes available in buffer. 
 *****************************************************************************/
uint16_t buffGetLevel(void)
{
  return buffer.currLevel;
}

/**************************************************************************//**
 * @brief   Adaption to input whole zero terminated strings 
 * @param   p - Pointer to first data byte.
 * @return  SUCCESS or FAIL(buffer filled)
 *****************************************************************************/
uint8_t buffStrIn(uint8_t *p)
{
  uint16_t l,i;
  uint8_t ret; 

  l = strlen((char *) p);

  if ( (buffer.currLevel + l ) > (BUFFER_SIZE-1) )
    return FAIL;

  ret = SUCCESS;
  for(i=0; i<l; i++)
  {
    if ( buffIn(*(p++)) == FAIL )
    {
      ret = FATAL;
      break;
    }
  }  
  return ret;
}

/**************************************************************************//**
 * @brief   input binary data (string) with defined length,
 *          '\0' (0x00) will not interpreted as string terminator
 * @param   p - Pointer to first data byte.
 * @return  SUCCESS or FAIL(buffer filled)
 *****************************************************************************/
uint8_t buffBinIn(uint8_t *p, uint8_t len)
{
  uint16_t i;
  uint8_t ret; 

  ret = SUCCESS;
  for(i=0; i<len; i++)
  {
    if ( buffIn(*(p++)) == FAIL )
    {
      ret = FATAL;
      break;
    }
  }
  return ret;
}


/**************************************************************************//**
 * @brief   Adaption to Output whole strings with fixed length of
 *          BUFFER_STR_SIZE.
 * @param   p - Pointer to first byte of output destination.
 * @return  SUCCESS or FAIL 
 *****************************************************************************/
uint8_t buffStrOut(uint8_t *p)
{
  uint8_t i,ret;
  #warning deprecated 
  /* ret = SUCCESS;
  for(i=0; i< BUFFER_STR_SIZE; i++)
  {
    if ( buffOut(p++) == FAIL )
    {
      ret = FAIL;
      break;
    }
  }
  */
  return ret;
}

//#define TEST_FIXED_STR_BUFFER 
#ifdef TEST_FIXED_STR_BUFFER 
#include <stdio.h>

int main(void)
{
  uint8_t ret,i,tvr, binDbg[130],tv[8] = {0x11, 0x22 , 0x33, 0x44, 0x55, 0x66, 0x77, 0x88};
  int cntr=0;
  char tStr[6][20] = {"Hallo\n", "", "welt!", "\r\n", "", "END"};

  for (i=0;i< 6; i++)
  {
    uint8_t r = buffStrIn(tStr[i]);

    printf(tStr[i]);

    if ( r == FAIL)
      printf("OK: BuffStrIn FAILs, Buffer full, i=%d\n",i);
  
    if ( r == FATAL)
      printf("Err: BuffStrIn return FATAL, i=%d\n",i);
  }

  //if (buffStrIn(tv) == SUCCESS)
  //    printf("Err: Buffer full and no FAIL returnd\n");

  printf("\n--OUTPUT raw, bytewise--\n");
  for (i=0;i<sizeof(tStr);i++)
  {
    uint8_t j,r;

    r = buffOut(&tvr);

    if (r == FAIL)
    {
      printf("\n\nOK: buffOut FAILs, Buffer empty, i=%d\n", i);
      break;
    }
 
    if ( r == FATAL)
      printf("\n\nErr: BuffStrOut return FATAL, i=%d\n",i);

    /*for (j=0;j<8; j++)
      if(tv[j] != tvr[j])
        printf("Err: Data not identcial outputted, j=%d\n", j);
    */

    printf("%c",tvr);
    binDbg[i] = tvr;
    cntr++;
  } 

  printf("\n-- --\n\nBufferLevel: %d, should be 0\n", buffGetLevel());

  printf("\n binary output:\n\n");

  for(uint16_t v=0; v<cntr; v++)
    printf("%02X",binDbg[v]);  
 
  printf("\n");

  for(uint16_t v=0; v<cntr; v++)
    printf("%2c",binDbg[v]);  
 
  printf("\nTest done\n");
}

#endif

