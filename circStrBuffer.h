/*!
 * \file circStrBuffer.h 
 * \brief general purpose circular buffer with 
 *        fixed byte-length string handling. 
 *
 * \author Droessler, Tobias
 * \date 30.04.2013
 ************************************************/ 

#include <stdint.h>

 /* == DEFINEs == */
#define BUFFER_SIZE          (512)            //  2^n (8, 16, 32, 64 ...)
#define BUFFER_MASK          (BUFFER_SIZE-1)   //	 

#define SUCCESS 	   (0)
#define FAIL		   ((uint8_t) -1)
#define FATAL              (1)

/* == PROTOTYPEs == */
/**************************************************************************//**
 * @brief   Adaption to input whole strings with fixed length BUFFER_STR_SIZE.
 * @param   p - Pointer to first data byte.
 * @return  SUCCESS or FAIL 
 *****************************************************************************/
uint8_t buffStrIn(uint8_t *p);

/**************************************************************************//**
 * @brief   input binary data (string) with defined length,
 *          '\0' (0x00) will not interpreted as string terminator
 * @param   p - Pointer to first data byte.
 * @return  SUCCESS or FAIL(buffer filled)
 *****************************************************************************/
uint8_t buffBinIn(uint8_t *p, uint8_t len);

/**************************************************************************//**
 * @brief   Adaption to Output whole strings with fixed length of
 *          BUFFER_STR_SIZE.
 * @param   p - Pointer to first byte of output destination.
 * @return  SUCCESS or FAIL 
 *****************************************************************************/
uint8_t buffStrOut(uint8_t *p);

/**************************************************************************//**
 * @brief   Returns current byte count inside buffer. 
 * @return  Total bytes available in buffer. 
 *****************************************************************************/
uint16_t buffGetLevel(void);

/************************************************************************//**
 * \brief	Get next byte out of buffer.
 * \param 	*pByte - destination pointer
 * \return	SUCCESS or FAIL 
 *			
 **************************************************************************/ 
uint8_t buffOut(uint8_t *pByte);

/* == INTERNAL PROTOTYPEs == */
static uint8_t buffIn(uint8_t byte); 


