/** @file signalLed.h 
  *   LED Signaling implementation for E271 NB IoT. 
  *   uses Systick Timer source clock to output LED signaling in 
  *   modes: 
  *
  *   use ledStausInit() to set the port pin to open drain output mode
  *
  *   use function call ledStatusSet(uint32_t state)
  *     with state:
  *
  *  LED_OFF           
  *  LED_ON            
  *  LED_BLINKSLOW     
  *  LED_BLINKFAST     
  *  LED_BLINKFASTFAST 
  *  LED_TOGGLE 
  *
  *  additionally there is a coded blinking to show states etc: 
  *       void ledStatusSetBlinkcode(uint8_t code)
  *
  *                             _   _       _   _
  *  example code =2 => LED   _| |_| |_____| |_| |_____..
  *                             _   _   _   _       _   _   _   _
  *          code =4 => LED   _| |_| |_| |_| |_____| |_| |_| |_| |___.. 
  *                           
  *                           0.2s/0.2ms coding+ 4x200ms idle(off)
  */ 

#ifndef SIGNALLED_HDR
#define SIGNALLED_HDR

#include "gpio.h"
#include <stdbool.h>

#define LED_OFF            0   ///< LED continuous off
#define LED_ON             1   ///< LED continuous on  
#define LED_BLINKSLOW      2   ///< LED blinking 0.5Hz aka 1s/on 1s off
#define LED_BLINKFAST      3   ///< LED blinking 1Hz aka 500ms on/500ms off
#define LED_BLINKFAST2     4   ///< LED blinking 5Hz aka 100ms on/100ms off
#define LED_BLINKFAST3     5   ///< LED blinking 10Hz aka 50ms on/50ms off
#define LED_BLINKFAST4     6   ///< LED blinking 50Hz aka 10ms on/10ms off
#define LED_BLINKCODED     10  ///< LED blinking coeded  
#define LED_TOGGLE         20  ///< toggle LED state


/** @brief set different signals on LED (green) 
  * @param please use predefined symbols
  */
void ledStatusSet(uint32_t state);

/** @brief init both leds NETLIGHT (blue) and STATUS (green)
  *        and apply test impuls (100ms) on both.   
  */
void ledStatusInit(void);

/** @brief set blink code for special blink mode LED_BLINKCODED.
  *        blinking will be repeated until other mode is set.  
  * 
  *  @param code:  results *code* pulses per cycle on the LED
  *  
  *  @param implizit actives LED blinking too, there is no need to call ledStatusSet separatly 
  *                             _   _       _   _
  *  example code =2 => LED   _| |_| |_____| |_| |_____..
  *                             _   _   _   _       _   _   _   _
  *          code =4 => LED   _| |_| |_| |_| |_____| |_| |_| |_| |___.. 
  *                           
  *                           0.2s/0.2ms coding+ 4x200ms idle(off)
  *                                
  */
void ledStatusSetBlinkcode(uint8_t code);
void ledBlueSetBlinkcode(uint8_t code);
/** @brief set different signals on LED (blue) 
  * @param please use predefined symbols LED_...
  */
void ledBlueSet(uint32_t state);

#endif // SIGNALLED_HDR
