/*
 * serialAsciiParser.h
 *
 *  Created on: 16.12.2015
 *      Author: droessler
 */

#ifndef SERIALASCIIPARSER_H_
#define SERIALASCIIPARSER_H_

#include <string.h>
#include <stdint.h>




#define PWM_CH_COUNT    6
#define DOUT_CH_COUNT   21
#define DIN_CH_COUNT    4
#define ADC_CH_COUNT    14

#define HAL_GET_ADC(ad) ad 
#define HAL_GET_DIN(ad) ad

#define HAL_SET_PWM(ad, v)
#define HAL_SET_DOUT(ad, v)

void parseCmd( char *pD);

#endif /* SERIALASCIIPARSER_H_ */
