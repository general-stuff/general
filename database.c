///
///      @file  database.c
///     @brief 	Database. Store Text strings and overwrite oldest entries
/// 			first. Strings are referenced by a ID, if there is no String
///             with a requested ID NULL will be returned. 
///				\n
///  			Every Entry is build up by data array, an active Byte and ID.
///				\n 			
///				ID = {0..0xFF}
///				max. number of entries: 16 (scale in header)
///
/// Detailed description starts here.
///
///    @author  Tobias Drößler (TDR), droessler.tobias@ik-elektronik.com
///
///  @internal
///    Created  28.05.2013
///   Revision  $Id: doxygen.cpp.templates,v 1.3 2010/07/06 09:20:12 mehner Exp $
///   Compiler  gcc/g++
///    Company  IK Elektronik
///  Copyright  Copyright (c) 2013, Tobias Drößler
///
/// This source code is released for free distribution under the terms of the
/// GNU General Public License as published by the Free Software Foundation.
///=====================================================================================
///


#include "database.h"
#include "string.h"

/* global Vars */

type_id_data base[ENTRIES];
uint8_t nextWriteIndex = 0;


/* FUNCTIONS */


/**
 * @brief   Init.: clear/deactivate all data entries.
 */
void initDatabase(void)
{
	uint8_t i; 

	for (i=0; i < ENTRIES; i++)
		base[i].active = false;

}


/**
 * @brief Searches data entry with ID id and returns pointer to first data
 *        byte. 
 * @param id - identifier   
 * @return pointer to first data byte, =NULL if entry not found.
 */
uint8_t* getData(uint8_t id)
{

	uint8_t i;

	for (i=0; i < ENTRIES; i++)
		if (base[i].id == id && base[i].active) 
			return base[i].data;

	return NULL;
}


/**
 * @brief   Insert new entry in the database. The oldest entry will be
 *          overwritten if there are no empty entryies.
 *          String length must be <= DATALENGTH_PER_ENTRY ! Longer strings will
 *          be truncated.  
 * @param   id - identifier to reference data later.
 * @param   pData - pointer to first byte of null-terminated string. 
 */
void setData(uint8_t id, uint8_t * pData)
{
	uint8_t i; 

	/* first: find if id exists and if, delete them */
	for (i=0; i < ENTRIES; i++)
		if (base[i].id == id && base[i].active )
		{
			base[i].active = false; 
		}

	/* append entry at write position */

	base[nextWriteIndex].id = id;
	base[nextWriteIndex].active = true;

    strncpy(base[nextWriteIndex].data, pData, DATALENGTH_PER_ENTRY - 1);	

	//	if (strlen(base[nextWriteIndex].data) > DATALENGTH_PER_ENTRY - 1) 
	   base[nextWriteIndex].data[DATALENGTH_PER_ENTRY-1] = '\0';

	/* forward write index */

	if (nextWriteIndex >= ENTRIES-1)
	  	nextWriteIndex = 0;
	else
		nextWriteIndex++;

}


