/*!
 * \file cbuffer.c 
 * \brief general purpose circular buffer implementation
 *
 * \author Droessler, Tobias
 * \date 30.04.2013
 ************************************************/ 

#include "cbuffer.h"

struct Buffer {
  uint8_t data[BUFFER_SIZE];  /**< data field */
  uint16_t read; 				/**< read index  */
  uint16_t write;				/**< write index */
  uint16_t currLevel;
} buffer = {  .read = 0, .write = 0, .currLevel = 0}; //needs C99 extentions enabled!
 
/************************************************************************//**
 * \brief	Put byte into buffer.
 * \param 	byte - byte value to put in
 * \return	SUCCESS or FAIL 
 *			
 ***************************************************************************/
uint8_t buffIn(uint8_t byte)
{
  uint16_t next = ((buffer.write + 1) & BUFFER_MASK);
  if (buffer.read == next)
    return FAIL;
  buffer.data[buffer.write] = byte;
  buffer.currLevel++;
  // buffer.data[buffer.write & BUFFER_MASK] = byte; // absolut Sicher
  buffer.write = next;
  return SUCCESS;
}

/************************************************************************//**
 * \brief	Get next byte out of buffer.
 * \param 	*pByte - destination pointer
 * \return	SUCCESS or FAIL 
 *			
 ***************************************************************************/ 
uint8_t buffOut(uint8_t *pByte)
{
  if (buffer.read == buffer.write)
    return FAIL;
  *pByte = buffer.data[buffer.read];
  buffer.read = (buffer.read+1) & BUFFER_MASK;
  buffer.currLevel--;
  return SUCCESS;
}

uint16_t buffGetLevel(void)
{
	return buffer.currLevel;
}