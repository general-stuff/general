/**
 *      @file  scheduler.c
 *      @brief  Scheduler für E209 - Pagermodul 
 *
 *
 *     @author  Tobias Drößler (TDR), droessler.tobias@ik-elektronik.com
 *
 *   @internal
 *     Created  13.06.2013
 *     Company  IK Elektronik
 *
 * =====================================================================================
 */

#include <stdint.h>
#include "scheduler.h"
#include <stdio.h>

#define uint8 uint8_t
#define uint32 uint32_t


/** \defgroup scheduler "Scheduler" */
/* @{ */

/* internal PROTOTYPES */

static void scheduler_clearTask(uint32 taskToClear);

/* module global variables */
uint32 tasks=0;


/* function declarations */
/**********************************************************//**
 * @brief   Abarbeitung nach prio. Reihenfolge. Höher-Prio. Task kann
 *          nieder-prio. Tasks nicht vor Ausführung stoppen. 
 *  
 * @note    Abarbeitung immer in Reihenfolge der Priorisierung.
 *          Kommt ein hoch priorisierter Task im Ablauf hinzu, wird dieser
 *          erst nach Abarbeitung der restlichen (nieder Prio.-) Tasks ausgeführt! 
 **************************************************************/

void scheduler_roundRobin(void)
{

    uint32 mask = 1;
    uint32 act_task;

    while (tasks != 0 && mask != 0)
    {
        act_task  = tasks & mask;
        scheduler_clearTask(mask);  // reset done task 

        switch (act_task)
        {
            case TASK_EWARN:
            printf("ewarn\n");
            {
                    static uint8 i = 0;
                    if (i < 2)
                    {
                    scheduler_setTask(TASK_EWARN);
                    i++;
                    }
                }
            break;

            case TASK_MIDNIGHT:
            printf("mid\n");
            break;
        
            case TASK_TIME:
            printf("time\n");
            break;  
        }



        mask = mask << 1;           // shift mask
    } 
}

/**********************************************************//**
 * @brief   Abarbeitung hart nach Priorität. D.h. hoch priorisierter Task kann
 *          nieder prior. Task völlig blockieren.
 *
 * @note    Abarbeitung beginnt IMMER mit höchst priorisiertem Task.
 *          Kommt während der Bearbeitung ein hoch prior. Task hinzu,
 *          wird dieser als nächstes ausgeführt.
 *          Somit können niedrig prior. Task stark verzögert bis garnicht
 *          ausgeführt werden. 
 *************************************************************/
void scheduler_hardPrio(void)
{

    uint32 mask = 1;
    uint32 act_task;

    while (tasks != 0 && mask != 0)
    {
        act_task = tasks & mask;

        if (act_task)
        {
            scheduler_clearTask(act_task);  // reset task at first
            mask = 1;                       // reset mask to high prio
            switch (act_task)
            {
                case TASK_EWARN:
                printf("ewarn\n");
                {
                    static uint8 i = 0;
                    if (i < 2)
                    {
                    scheduler_setTask(TASK_EWARN);
                    i++;
                    }
                }
                break;

                case TASK_MIDNIGHT:
                printf("mid\n");
                break;
        
                case TASK_TIME:
                printf("time\n");
                break;  
            }
    
        } else {
            mask = mask << 1;         // shift mask
        }
    }
}


/**********************************************************//**
 * @brief   Unterbrechungsfreies setzten eines Tasks
 * @param   Task-Maske 
 * @return  Ergebnis:   1 => Task gesetzt. \n
 *                      0 => Fehler: Task war bereits gesetzt . 
 *************************************************************/
uint8_t scheduler_setTask(uint32_t taskToSet) 
{
    if ( tasks & taskToSet )
        return 0;
    else
    {

        // globales IRQ Verbot!
        tasks |= taskToSet;
        // globale IRQs reaktiv. 
        return 1;
    }
}


/**********************************************************//**
 * @brief   Unterbrechungsfreies löschen eines Tasks
 * @param   Task-Maske
 *************************************************************/
static void scheduler_clearTask(uint32_t taskToClear)
{
    // globales IRQ Verbot!
    
    tasks &= ~taskToClear;
    
    // globale IRQs reaktiv. 
}


#define TEST
#ifdef TEST

void main(void)
{

    printf("Test scheduler_roundRobin\n"); 
    scheduler_setTask(TASK_EWARN);
    scheduler_setTask(TASK_TIME);
    scheduler_setTask(TASK_MIDNIGHT);


    scheduler_roundRobin();
    scheduler_roundRobin();
    scheduler_roundRobin();

    printf("\nTest scheduler_hardPrio\n");
    scheduler_setTask(TASK_EWARN);
    scheduler_setTask(TASK_TIME);
    scheduler_setTask(TASK_MIDNIGHT);

    scheduler_hardPrio() ;
    scheduler_hardPrio() ;
    scheduler_hardPrio() ;
    scheduler_hardPrio() ;


}
#endif

/* @} */
