/**!
 *       @file  enocean_switch_coder.c
 *      @brief  
 *
 *
 *     @author Tobias Drößler, IK Elektronik GmbH 
 *
 *   @internal
 *     Created  28.10.2014
 *    Compiler  gcc/g++
 *
 * This source code is released for free distribution under the terms of the
 * GNU General Public License as published by the Free Software Foundation.
 * =====================================================================================
 */


#include "enocean_switch_encoder.h"

/*  INTERNAL PROTOs   */
uint8_t calcHash(uint8_t *ptr_data_buff);
uint16_t insertInvSync( uint8_t inp);


/**
 * @brief    |4b RORG 4b data| 4b data */
void eo_encode(uint8_t *id_ptr, uint8_t rorg, uint8_t data, uint8_t *destBuff )
{
	uint8_t     data_buff[6] = {0};
    uint8_t     teleg_buff[12] = {0};
    uint16_t    u16tmp;

    data_buff[0] = ((rorg << 4) | (data >> 4)) ;
    data_buff[1] = ((data << 4) | (*id_ptr >> 4)) ;
    data_buff[2] = (*id_ptr << 4);
    data_buff[2] |= (*(++id_ptr) >> 4);
    data_buff[3] = (*id_ptr << 4);
    data_buff[3] |= (*(++id_ptr) >> 4);
    data_buff[4] = (*id_ptr << 4);
    data_buff[4] |= (*(++id_ptr) >> 4);
    data_buff[5] = ((*id_ptr << 4));
    data_buff[5] |= calcHash(&data_buff[0]);

    // telegram data, with inv and sync

    teleg_buff[0] = 0xAA;                       // PRE
    u16tmp = insertInvSync(data_buff[0]);
    teleg_buff[1] = 0x90 | (u16tmp >> 8);       // SOF
    teleg_buff[2] = u16tmp & 0xFF;

    for (uint8_t i= 0; i <3; i++)
    {
        u16tmp = insertInvSync(data_buff[2*i+1]);
        uint16_t u16tmp2 = insertInvSync(data_buff[2*i+2]);

        teleg_buff[3*i+3] = ((u16tmp & 0xFF0) >> 4);
        teleg_buff[3*i+4] = (((u16tmp & 0xF) << 4)  | ( (u16tmp2 & 0xF00) >> 8)) ;
        teleg_buff[3*i+5] = (u16tmp2 & 0xFF);

    }

    teleg_buff[10] &= 0xC0;         // clear last sync  ... and ...
    teleg_buff[10] |= 0x2F;         // add EOF: End Of Frame 
    
    // copy to destination puffer

    for (uint8_t i = 0; i < 11; i++)
    {
        *destBuff++ = ~teleg_buff[i];
    }
}

uint8_t calcHash(uint8_t *ptr_data_buff)
{
	uint8_t i,hash=0;
	for (i=0; i<6; i++)
	{
		hash += *ptr_data_buff++;
	}
	hash += (hash >> 4);
    return (hash & 0x0F);
}


uint16_t insertInvSync( uint8_t inp)
{
    uint16_t b=0;

    b  = ((inp & 0xE0) << 4);
    b |= (((~b) & 0x200)  >> 1);
    b |= ((inp & 0x1C) << 3);
    b |= (((~b) & 0x20)  >> 1);
    b |= ((inp & 0x03) << 2);
    b |= 0x01;      

    return (b & 0x0FFF);    
}

#define TEST_ENCODER
#ifdef TEST_ENCODER 

#include <stdio.h>

void main(void)
{

 uint8_t testOut[15]= {0};
 /* +++             -- ID ---------  RORG|-data-|--- output ---- ..*/
 uint8_t test1[] = {0x03,0xdf,0,0x0A, 5  , 0x35,  0x55, 0x6a, 0x62, 0xA6, 0xed, 0x1a, 0x16,0xee,0xee,0x5d,0x10 };
 uint8_t test2[] = {0x03,0x80,0,0x01, 5  , 0x25,  0x55, 0x6a, 0x66, 0xA6, 0xed, 0x2e, 0xee,0xee,0xee,0xe2,0xd0 };


 eo_encode( test1, test1[4], test1[5], testOut);
 
 for (uint8_t i= 0; i< 11; i++)
 {
    // printf( "%X ", testOut[i]);
	if ( testOut[i] != test1[i+6])
		printf("test1 failed at output byte %d\n", i);
 }

 eo_encode( test2, test2[4], test2[5], testOut);

 for (uint8_t i= 0; i< 11; i++)
 {
    // printf( "%X ", testOut[i]);
	if ( testOut[i] != test2[i+6])
		printf("test2 failed at output byte %d\n", i);
 }
 printf("done\n");

}
#endif 

