
#define TEST 

#ifdef TEST 
#include <stdio.h>
#endif 
#include <stdint.h> 
#include "dayofweek.h"

/**************************************************************************//**
 * @brief   Weekday Calculation - does not check if date exists. 
 * @param   day   - 1..31
 * @param   month - 1..12
 * @param   year  - ??
 * @return  0 - Mo, 1 - Di, .. 6 - Su
 *****************************************************************************/
uint8_t dayofweek(uint8_t day, uint8_t month, uint16_t year)
{
   /** Zeller's congruence for the Gregorian calendar. **/
   /** With 0=Monday, ... 5=Saturday, 6=Sunday         **/
   if (month < 3) {
      month += 12;
      year--;
   }
   return ((13*month+3)/5 + day + year + year/4 - year/100 + year/400) % 7;
}


#ifdef TEST 

int main(void)
{
  uint8_t i,ret = 0;
  uint16_t tv[][4] = 
  {
    {1, 1, 2000, 5} ,
    {23,8, 2001, 3} ,
    {1, 2, 2022, 1} , 
    {31,12,2013, 1} ,
    {18, 9,2013, 2} ,
    {31, 2,2013, 6} ,   // not valid date
    {10, 4,2072, 6} ,
    {11,11,2066, 3} , 
    { 12,6,1989, 0} ,
    { 2, 10,1986,3} 
  }; 

  printf("Testroutine of dayofweek(..)");

  for(i=0;i<sizeof(tv)/sizeof(tv[0]);i++)
  {
    if ( dayofweek(tv[i][0], tv[i][1], tv[i][2] ) != tv[i][3])
      ret++;

  }
  printf("\nFailure Count: %u/%u", ret, sizeof(tv)/sizeof(tv[0]));
  printf("\n -- END --");
}
#endif 
