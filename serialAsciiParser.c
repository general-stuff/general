/*
 * serialAsciiParser.c
 *
 *  Created on: 16.12.2015
 *      Author: droessler
 */

#include "serialAsciiParser.h"
#include "stdio.h"
static char argv[5][20] = {0};  // global var for cmd string vector


void cmdSet(void);
void cmdGet(void);
void cmdSetPwm(uint32_t ad, uint32_t v);
void cmdSetDout(uint32_t ad, uint32_t v);

//#define TEST
#ifdef TEST 



int main(void)
{
<<<<<<< HEAD
  uint8_t k=0, v,tv[10][20]={{0}};  
=======
  uint8_t k=0,tv[20][20]={{0}};  
>>>>>>> 4404763439745bbb36e19712654d705cc14809e8
	strcpy(tv[k++],"set pwm3 1");
	strcpy(tv[k++],"set pwm3 1");
	strcpy(tv[k++],"set 23 0");
	strcpy(tv[k++],"set pwm2 12");
	strcpy(tv[k++],"get 0");
	strcpy(tv[k++],"if r uart2 ");
	strcpy(tv[k++],"if w uart3 ");
<<<<<<< HEAD
  strcpy(tv[k++],"");
  strcpy(tv[k++]," ");

  v = k;
  for (k=0; k<v; k++)
  {
   parseCmd(&tv[k][0]);
   printf("---\n");
  }

  return 0;
}
=======
	strcpy(tv[k++],"set pwm2 102");
	strcpy(tv[k++],"get 2 ");
	strcpy(tv[k++],"get 2 2");
	strcpy(tv[k++],"get adc2 ");

  for (k=0; k<20; k++)
  {
      printf("-- %s --\n", tv[k]);
      parseCmd(&tv[k][0]);

      printf("------\n");
>>>>>>> 4404763439745bbb36e19712654d705cc14809e8

  }
  return 0;
}
#else
#include <stdarg.h>
#include "HL_sci.h"
  int printf(const char *p, ...)
  {
	   char iBuff[100];
	   va_list arglist;
	   va_start(arglist, p);
	   vsprintf( iBuff, p, arglist );
	   va_end( arglist );
	  // vsprintf(iBuff, p, arglist);
	   sciSend(sciREG1, strlen(iBuff), (uint8_t *) iBuff);
  };
#endif



void parseCmd( char *pD)
{
	uint8_t i =0;
	char *tp;

  argv[0][0] = '\0'; // clear buffer 
  argv[1][0] = '\0'; // clear buffer 
  argv[2][0] = '\0'; // clear buffer 
  
  
  tp = strtok(pD, " ;\n\r");
  while (i<5)
  {
    if (tp != NULL)
    {
      strcpy(argv[i++],tp);
    } else
    {
      break;
    }
    tp = strtok(NULL, " ;\n\r");
  }
  printf("  db:argv %s %s %s %s\n", argv[0], argv[1], argv[2], argv[3]);
  
  if (strcmp(argv[0], "set") == 0)
	{
	  printf("setter cmd\n");
      cmdSet();
	}
	else if (strcmp(argv[0], "get") == 0)
	{
	  printf("getter cmd\n");
      cmdGet();
	}
	else if (strcmp(argv[0], "if") == 0)
	{
	  printf("interf. cmd\n ");
	}
	else
		printf("cmd <%s> is unkown!\n", argv[0]);
}

/** 
 *   two args needed: address and value 
 */
void cmdSet(void)
{
  uint32_t r,addr,value;
  char *pr;
  pr = strstr(argv[1], "pwm");
  if (pr != NULL)
  {
    printf("found pwm setter\n");
    r = sscanf(&(argv[1][3]), "%d", &addr );
    if (r)
    {
      printf("  db:addr:%d\n", addr );
      r = sscanf(&argv[2][0], "%d", &value );
      if ((r) && (addr>0) || (addr<=PWM_CH_COUNT))
      {
        printf("  db:value:%d\n", value);
        if (value < 100)
          cmdSetPwm(addr, value);
        else
          printf("value to set is invalid, range: 0..100[%%]\n");
      }
    }     
  }
  else 
  {
    r = sscanf(&argv[1][0], "%d", &addr);

    if (r==1)
    {
      printf("  db:addr:%d\n", addr );
      r = sscanf(&argv[2][0], "%d", &value);


      if (r==1)
      {
        printf("  db:value:%d\n", value );
        if (addr <= DOUT_CH_COUNT)
        {
          if((value != 0))
           value = 1;
          cmdSetDout(addr, value);
        }
      }  
    }
  }
}

void cmdSetPwm(uint32_t adr, uint32_t v)
{
  HAL_SET_PWM(adr, v);
  printf("PWM: set ch #%d at %d%% duty\n", adr, v);
}

void cmdSetDout(uint32_t ad, uint32_t v)
{
  HAL_SET_DOUT(adr, v);
  printf("DOUT: set dout #%d to %d\n", ad, v);
}

/** 
 *   one arg needed: address  
 */
void cmdGet(void)
{
  uint32_t r,addr,value;
  char *pr;
  pr = strstr(argv[1], "adc");
  if (pr != NULL)
  {
    printf("found ADC getter\n");
    r = sscanf(&(argv[1][3]), "%d", &addr );
    if (r)
    {
      printf("  db:addr:%d\n", addr );
      if ((addr>0) && (addr<=ADC_CH_COUNT))
      {
        printf("adc%d = %fV\n", addr, HAL_GET_ADC(addr)); 
      }
      else
        printf("invalid address parameter\n");
    }     
  }
  else 
  {
    r = sscanf(&argv[1][0], "%d", &addr);
    if (r==1)
    {
      printf("  db:addr:%d\n", addr );
      if ((addr>0) && (addr<=DIN_CH_COUNT))
      {
         printf("din%d = %d\n", addr, HAL_GET_DIN(addr));
      }
      else
         printf("invalid address parameter\n");
    }
  }
}


