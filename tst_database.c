

//#include "unity_fixtures.h"


#include "database.h"
#include "unity.h"


	uint8_t i;
	uint8_t a[] = "Hallo Welt!";
	uint8_t b[] = "WeltWeltWeltWeltW";
	uint8_t c[] = "WdfsdfsdfsfsffsdfsfdfsfdsfdfsfsffsfdsdffaffffffaeltWeltWeltWeltW";
	uint8_t d[] = "";
	uint8_t *ptemp;
	uint8_t temp[160];


void setUp(void)
{
}

void tearDown(oid)
{
}


void test_general(void)
{

	i = 0;
	setData(i, a);
	ptemp = getData(i);
	TEST_ASSERT_EQUAL_STRING(a, ptemp);

	i = 0xFF;
	setData(i, a);
	ptemp = getData(i);
	TEST_ASSERT_EQUAL_STRING(a, ptemp);
}

void test_overwrite_sameIndex(void)
{

	for (i=0;i <= ENTRIES; i++)
		setData(i, a);
	
	
	i--;


//	setData(i,a);
	ptemp = getData(i);
	TEST_ASSERT_EQUAL_STRING(a, ptemp);
	
	setData(i,b);
	ptemp = getData(i);
	TEST_ASSERT_EQUAL_STRING(b, ptemp);

}


void test_user_sim(void)
{


	for (i=0;i <= ENTRIES; i++)
	{
		setData(i*2, c);

		ptemp = getData(i*2);
		TEST_ASSERT_EQUAL_STRING(c,ptemp);
		
		ptemp = NULL;
	}
}


void test_overlength(void)
{

	uint8_t ov_str[200], get_str[200];


	strcpy (ov_str,"");

	get_str[0] = '\0';

	TEST_ASSERT_EQUAL_STRING(ov_str,get_str);

	for (i =0; i< 199; i++)
		ov_str[i] = 'x';
	
	ov_str[199] = '\0';

	setData(44, ov_str);

	ptemp = getData(44);

	strcpy(get_str,ptemp);

	ov_str[159] = '\0';

	TEST_ASSERT_EQUAL_STRING(ov_str,get_str);  
	TEST_ASSERT_EQUAL_STRING(ov_str,ptemp);  

//	TEST_ASSERT_EQUAL_MEMORY(ptemp,ov_str, DATALENGTH_PER_ENTRY-2);
//	TEST_ASSERT_EQUAL_UINT8(get_str[DATALENGTH_PER_ENTRY-1],'\0');

}

void test_strNotSet(void)
{
	initDatabase();
	
	for (i=0; i<20; i++)
	{
		TEST_ASSERT_NULL(getData(i));
	}
}


/* strncpy not implicitly appends '\0' !!!! */
/* demonstration of the issue */
void test_strncpy(void)
{

	uint8_t source[10] = "123456789";
	uint8_t i,dest[8];


	for (i=0; i<8; i++)
		dest[i] = 'a';


	printf("===\n");
	printf(source);

	printf("\n");
	printf(dest);

	printf("\n");
	strncpy (dest, source, 8);
	dest[7] = '\0';
	printf(dest);

	printf("\n");
	printf("===\n");


}
