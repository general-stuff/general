
#include <stdint.h>

uint8_t calcCrc8(uint8_t *pData, uint8_t len);

uint8_t calcCrc8(uint8_t *pData, uint8_t len)
{
    uint8_t pn = 0xEA; //0x07; 		/*Polynom: 0x07*/
    uint8_t crcState = 0; 	/*Seedvalue: 0 */

    uint8_t val,i,j;
    i = 0;
    while ( i<len)
	{
	    val = *(pData++);
		crcState = crcState ^ val;
		for (j=0;j<8;j++)
		{
		    if (crcState & 0x80)
			{ 
			    crcState = ((crcState << 1) ^ pn);
			}
			else
			{
			    crcState = crcState << 1;
			}
		}
		i++;
	}
	
	return crcState;
}



#define CRC8_TEST
#ifdef CRC8_TEST
#include <stdio.h>

int main(void)
{
	uint8_t in[]  = { /*-------data------------------|CRC8 */
						0x00, 0x00, 0x00, 0x18, 0x01, 0x19,
						0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
						0x00, 0x00, 0x00, 0x02, 0x00, 0x02,
						0x05, 0xE2, 0x3B, 0x38, 0x00, 0xF9,
						0x00, 0x00, 0x00, 0x6A, 0x01, 0x70,
						0x00, 0x00, 0x00, 0x6A, 0x00, 0x77,
						0x12, 0x8D, 0x09, 0xDD, 0x01, 0x57,
						0x12, 0x8D, 0x09, 0xDD, 0x00, 0x50,
						0x00, 0x00, 0x00, 0x03, 0x00, 0x03,
						0x00, 0x00, 0x00, 0x06, 0x00, 0x06};
	
	
	uint8_t *p = &in[0];
	uint8_t i;
	printf ("Test of CRC8 implementation: %d Testvectors found\n", sizeof(in)/6);
	for ( i = 0; i < sizeof(in)/6; i++)
	{
	   if ( calcCrc8(p, 5) != *(p+5) )
	      printf("Fail, line: %d - result %X, should %X\n", i, calcCrc8(p, 5), *(p+5));
	   
	   p = p+6;
	}
    printf ("done.\n");


 	printf("Collisiontest:\n");
    {
        uint8_t collisionCount, data[]  = {0x00, 0x00, 0x00, 0x18, 0x01};   
        uint8_t crc_fix = calcCrc8(data, 5);

        collisionCount=0;
        printf("\tSchaltzustand 0x01.\n");
        for ( i = 0; i < 0xFF; i++)
        {
          data[3] = i;  
          if (calcCrc8(data, 5) == crc_fix )
          {
            collisionCount++;
            printf("\tEqual, value: 0x%X \n", i);
          }
        }

        printf("\tSchaltzustand 0x00.\n");
        data[4] = 0;
        for ( i = 0; i < 0xFF; i++)
        {
          data[3] = i;  
          if (calcCrc8(data, 5) == crc_fix )
          {
            collisionCount++;
            printf("\tEqual, value: 0x%X \n", i);
          }
        }

		{
			uint8_t pd[] = { 0x00, 0x00, 0x00, 0x02, 0x00,
							 0x00, 0x00, 0x00, 0x04, 0x00,
							 0x00, 0x00, 0x00, 0x08, 0x00,
							 0x00, 0x00, 0x00, 0x0C, 0x00,
							 0x00, 0x00, 0x00, 0xC0, 0x00,
							 
			};
			uint8_t *p = pd;
			
			printf("\n Check bitsync/bitshift behaviour:\n");
			
			for (uint8_t l=0; l< sizeof(pd)/5; l++)
			{
				uint8_t origCRC = calcCrc8(p,5);
				printf("\n");
				*(p+3) = (*(p+3))>>1;
				printf("CRC: 0x%X -- 0x%X", origCRC, calcCrc8(p,5));
				if ((origCRC>>1) == calcCrc8(p,5))
					printf("Err - data=0");
				
				*(p+4) = 1;
				*(p+3) = (*(p+3))<<1;
				origCRC = calcCrc8(p,5);
				
				*(p+3) = (*(p+3))>>1;
				printf("CRC: 0x%X -- 0x%X", origCRC, calcCrc8(p,5));
				if ((origCRC>>1) == calcCrc8(p,5))
					printf("Err - data=1");				
			
				p = p + 5;
			}
		}
    }  


	return 0;

}

void shiftBitwise(uint8_t *pData, uint8_t left, uint8_t byteCount)
{
	uint8_t over;

	if (left)
	{
		for (uint8_t i=byteCount; i>0;i++)
		{

			
		}

	}
	else
	{

	}


}

#endif
